'use strict'

/** // ## Задание
 *    Дан массив books.
 *    - Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
 *    - На странице должен находиться `div` с `id="root"`, куда и нужно будет положить этот список
 *    (похожая задача была дана в модуле basic).
 *    - Перед выводом обьекта на странице, нужно проверить его на корректность
 *    (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету,
 *    в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
 *    - Те элементы массива, которые являются некорректными по условиям предыдущего пункта,
 *    не должны появиться на странице.
 *
 *    #### Примечание
 *    Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
 *
 *    #### Литература:
 *    - [Перехват ошибок, "try..catch"](https://learn.javascript.ru/exception)
 *    - [try...catch на MDN](https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Statements/try...catch)
 **/

// ## Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию `try...catch`.

/**
 *   При розробці програми, щоб явно вказати дані якого типу повинні бути використані - діапозон, одиниця виміру то що,
 *   при яких стандартні помилки не виникають, а для програми це критично.
 *   Можна створити виключення при введенні даних користувачем
 *   При отриманні відповіді з сервера, якщо дані не відповідають тому, в якому вигляді їх очікує програма
 **/

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.getElementById('root');
const ul = document.createElement('ul')
root.append(ul)

class ListBuild {
    constructor(element) {
        this.listElement = element
        this.textList = []
    }

    static createListItem(text) {
        const li = document.createElement('li')
        li.textContent = text;
        return li;
    }

    update() {
        while (this.listElement.firstChild) {
            this.listElement.removeChild(this.listElement.firstChild)
        }
        for (const text of this.textList) {
            this.listElement.appendChild(ListBuild.createListItem(text));
        }
    }

    createArr(array) {
        for (const key of array) {
            try {
                if (key.name && key.author && key.price) {
                    for (const i in key) {
                        this.textList.push(`${i} : ${key[i]}`)
                    }
                } else {
                    throw new Error('об\'єкт масиву не войшов у список, так як містить не всі властивості')
                }
            }
            catch (e) {
                console.log(e.message)
            }
        }
        return this.textList
    }

    add(array) {
        this.createArr(array)
        this.update()
    }
}

new ListBuild(ul).add(books)

