((D) => {

    class Create {
        constructor(atr) {
            this.atr = atr
            this.table = D.createElement('p')
            this.div = D.querySelector('.container')
            this.div.prepend(this.table)
            this.table.setAttribute('class', this.atr)
        }
    }

    class CreateTable extends Create {
        constructor(atr) {
            super(atr)
            for (let i = 1; i <= 10; i++) {
                for (let j = 1; j <= 10; j++) {
                    let td = document.createElement('div')
                    td.classList.add('col')
                    td.setAttribute('act', '0')
                    this.table.appendChild(td).dataset.ij = `${i}.${j}`
                }
            }
        }
    }

    class CreateMenu extends Create {
        constructor(atr) {
            super(atr)
            this.start = D.createElement('button')
            this.level = D.createElement('select')
            this.gamerScore = D.createElement('pre')
            this.gamer = D.createElement('p')
            this.pcScore = D.createElement('pre')
            this.pc = D.createElement('p')
            const [, , , ...value] = Object.values(this),
                [, , , ...key] = Object.keys(this)
            value.forEach((e, i) => {
                D.querySelector('.menu').appendChild(e).classList.add(key[i])
                e.innerText = key[i].replace(/([A-Z])/g, ' $1')
                    .replace(/[A-Z]/g, (u) => u.toLowerCase())
            })
            for (let i = 0.5; i <= 1.5; i += 0.5) {
                let option = D.createElement('option')
                option.setAttribute('value', `${i}`)
                this.level.appendChild(option).innerHTML = i + ' sec'
            }
        }
    }

    class CreateArr {
        constructor() {
            this.arr = []
            for (let i = 1; i <= 10; i++) {
                for (let j = 1; j <= 10; j++) {
                    this.arr.push({"i": i, "j": j})
                }
            }
        }
    }

    let pc = 0, gamer = 0, timerId, arr = new CreateArr

    class GetRandomInt {
        constructor() {
            this.min = Math.ceil(arr.arr.length-1);
            this.max = Math.floor(0);
            this.a = this.get()
            this.b = arr.arr.splice(this.a, 1)
        }
        get() {
            return Math.floor(Math.random() * (this.max - this.min + 1)) + this.min
        }
    }

    class SetCell extends GetRandomInt {
        set() {
            D.querySelector(`[data-ij="${this.b[0].i}.${this.b[0].j}"]`)
                .setAttribute('act', '1')
        }
    }

    new CreateTable('block col')
    new CreateMenu('menu col')

    function go(x) {
        timerId = setInterval(() => {
            new SetCell().set()
            setTimeout(() => {
                if (D.querySelector('[act="1"]')) {
                    D.querySelector('[act="1"]').setAttribute('act', '3')
                    pc++
                }
                update()
            }, x * 1000-20);
        }, x * 1000)
    }

    function exit(unit, message) {
        if (unit >= 50) {
            clearInterval(timerId)
            alert(message)
        }
    }

    function update() {
        D.querySelector('.gamer').innerText = gamer
        D.querySelector('.pc').innerText = pc
        exit(gamer, 'Ви виграли!')
        exit(pc, 'Ви Програли')
    }

    function clear() {
        arr = new CreateArr
        pc = 0;
        gamer = 0
        D.querySelectorAll('[act]').forEach(e => e.setAttribute('act', '0'))
        D.querySelector('.gamer').innerText = '0'
        D.querySelector('.pc').innerText = '0'
    }

    D.querySelector('.start').addEventListener('click', () => {
        clear()
        go(D.querySelector('.level').value)
    })

    D.querySelector('.level').addEventListener('change', () => {
        clearInterval(timerId)
        clear()
    })

    D.querySelector('.block').addEventListener('click', e => {
        if (+e.target.getAttribute('act') === 1) {
            e.target.setAttribute('act', '2')
            gamer++
        }
    })

})(document)