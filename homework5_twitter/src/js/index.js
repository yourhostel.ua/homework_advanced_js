( async ( D ) => {

    const element = D.querySelector('.main');

    class Card {
        constructor( email, name, title, body, id, element ) {
            this.email = email
            this.name = name
            this.title = title
            this.id = id
            this.body = body
            this.element = element
        }

        createCard() {
            this.element.innerHTML = `   
            <div class="head">
                <img class="photo" src="" alt="user photo">
                <div class="bod">
                    <div class="user">
                <span class="user_name">${ this.name }</span>
                    <button class="delete" data-id="${ this.id }">x</button>
                    </div>
                <h3>${ this.title }</h3>
                </div>
            </div>
            <p class="text">${ this.body }</p>
            <p><a href="${ this.email }">${ this.email }</a></p>`;
        }
    }

    async function getLoad( url ) {
        let response = await fetch( url ), result = await response.json();
        return result.map(e => {
            const { id, name, email, userId, title, body } = e;
            return { id, name, email, userId, title, body }
        })
    }

    async function deleteLoad( url ) {
        let response = await fetch( url ,{ method: 'DELETE' });
        if ( response.ok ){
            return  response.ok
        } else return false
    }

  const users = await getLoad('https://ajax.test-danit.com/api/json/users'),
        posts = await getLoad('https://ajax.test-danit.com/api/json/posts'),
        concatPosts = posts.map( e => {
        const { id: idPost , userId, title, body } = e;
        let fullPost
        users.forEach( e => {
            const { id, name, email } = e;
            if ( userId === id ) {
                fullPost = { idPost, name, email, title, body }
            }
        })
        return fullPost
    });

    concatPosts.forEach( e => {
        let elem = D.createElement( 'div' )
        element.appendChild( elem ).classList.add( 'card' )
        elem.setAttribute( 'data-card-id', e.idPost )

        new Card( e.email, e.name, e.title, e.body, e.idPost, elem ).createCard()
    })

    D.querySelector( '.main' )
        .addEventListener( 'click', async e => {
            if ( e.target.classList.contains( 'delete' ) &&
                await deleteLoad( `https://ajax.test-danit.com/api/json/posts/${ e.target.getAttribute('data-id') }` ) )
            {
                D.querySelector(`[data-card-id='${ e.target.getAttribute('data-id') }']`).remove()
            }
        });

})( document );
