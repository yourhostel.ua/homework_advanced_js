'use strict'
/**
*   Теоретический вопрос
*   Обьясните своими словами, как вы понимаете, что такое деструктуризация и зачем она нужна
*
*   Деструктуризация, spread, rest
*
*   Все задания нужно выполнить, используя синтаксис деструктуризации
**/
// деструктуризація це прийом спрощення присвоєння/обміну змінними

// ...spread оператор розповсюдження - дозволяє передати масив параметрів у функцію може використовуватися кілька разів
// на відміну від rest. Так само як і деструктуризація викликає інтерфейс ітератора Symbol.iterator, тому може працювати
// з будь-якими об'єктами, що ітеруються - наприклад, можна конкатенувати масиви або об'єкти або строки:
const a ="spread", b ="rest", d = [...a, ...b]
console.log(d, 'приклад у теорії')
// ...rest залишковий параметр дозволяє передати параметри у функцію коли не відомо скільки їх буде. Використовується
// завжди один та як останній параметр з трикрапкою перед ім'ям змінної. На відміну від arguments у тілі функції можемо
// працювати методами як із повноцінним масивом

//                                  Завдання 1
/**
*  Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
*  У вас есть 2 массива строк, в каждом из них - фамилии клиентов. Создайте на их основе один массив,
*  который будет представлять собой объединение двух массив без повторяющихся фамилий клиентов.
**/

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//реалізація:

const clients = Array.from( new Set( [ ...clients1, ...clients2 ] ) )
const newArr = [ ...new Set( [ ...clients1, ...clients2 ] ) ] //варіант ментора
console.group('Завдання 1')

console.log( Array.isArray( new Set( [ ...clients1, ...clients2 ] ) ),'без Array.from не масив')
console.log( Array.isArray( clients ), 'clients - масив' )
console.log( clients,'new Set - виключив дублікати' )

console.groupEnd()

console.log('___________________________________')

//                                  Завдання 2
/**
* Перед вами массив `characters`, состоящий из объектов. Каждый объект описывает одного персонажа.
*
* Создайте на его основе массив `charactersShortInfo`, состоящий из объектов, в которых есть только 3 поля
* - name, lastName и age.
**/

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
]

//реалізація:

const charactersShortInfo = characters.map( e => {
  const { name, lastName, age } = e   // деструктуруємо поточний об'єкт масиву characters, присвоюємо значення необхідних полів
  return { name, lastName, age }      // повертаємо новому масиву charactersShortInfo укорочений варіант об'єкту
})

console.group('Завдання 2')

console.log(charactersShortInfo, 'коротка версія масиву characters')

console.groupEnd()
console.log('___________________________________');

//                                  Завдання 3
/**
* У нас есть объект `user`
* Напишите деструктурирующее присваивание, которое:
* - свойство name присвоит в переменную name
* - свойство years присвоит в переменную age
* - свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
* Выведите переменные на экран.
**/
    (()=> {
      const user = {
        name: "John",
        years: 30
      };

//реалізація:

//деструктуруємо user, присвоюємо змінній age значення years,
// задаємо дефолтне значення змінної isAdmin = false якщо в об'єкті поле відсутнє
      const { name, years: age, isAdmin = false } = user

      console.group('Завдання 3')

      console.log( `name: ${name} age: ${age} isAdmin: ${isAdmin}` )
      console.log( user )

      console.groupEnd()
      console.log('___________________________________')
    })()
//                                  Завдання 4
/**
* Детективное агентство несколько лет собирает информацию о возможной личности [Сатоши Накамото]
* (https://ru.wikipedia.org/wiki/Satoshi_Nakamoto)
* Вся информация, собранная в конкретном году, хранится в отдельном объекте.
* Всего таких объектов три - `satoshi2018`, `satoshi2019`, `satoshi2020`.
*
* Чтобы составить полную картину и профиль, вам необходимо объединить данные из этих трех объектов в один объект -
* `fullProfile`.
*
* Учтите, что некоторые поля в объектах могут повторяться. В таком случае в результирующем объекте должно
* сохраниться значение, которое было получено позже (например, значение из 2020 более приоритетное по сравнению с 2019).
*
* Напишите код, который составит полное досье о возможной личности Сатоши Накамото.
* Изменять объекты `satoshi2018`, `satoshi2019`, `satoshi2020` нельзя.
**/

const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422,
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto',
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

//реалізація:

const satoshiAllInfo = {...satoshi2018,...satoshi2019,...satoshi2020}

console.group('Завдання 4')
console.log(satoshiAllInfo, 'з повторюваних властивостей перемогли останні')

console.groupEnd()
console.log('___________________________________')

//                                  Завдання 5
/**
* Дан массив книг. Вам нужно добавить в него еще одну книгу, не изменяя существующий массив
* (в результате операции должен быть создан новый массив).
**/

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const books2 = [...books,bookToAdd]

console.group('Завдання 5')
console.log(books2, 'новий масив включає всі книги масиву books + книга bookToAdd')

console.groupEnd()
console.log('___________________________________');

//                                  Завдання 6
/**
* Дан обьект `employee`. Добавьте в него свойства age и salary, не изменяя изначальный объект
* (должен быть создан новый объект, который будет включать все необходимые свойства).
* Выведите новосозданный объект в консоль.
**/
(() => { //обгортка лише за для ізоляції конфліктуючих змінних name та age
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

// const { name, surname, age = 0, salary = 0 } = employee
//   const  newEmployee = { name, surname, age, salary }
  const newEmployee = { ...employee, salary: 0, age: 0 }
  console.group('Завдання 6')
  console.log( newEmployee, 'додані age та salary' )

  console.groupEnd()
})()
console.log('___________________________________')

//                                  Завдання 7
/**
* Дополните код так, чтоб он был рабочим
**/

const array = ['value', () => 'showValue'];

const [value, showValue] = array

console.group('Завдання 7')
console.log(value); // должно быть выведено 'value'
console.log(showValue());  // должно быть выведено 'showValue'

console.groupEnd();
console.log('___________________________________');
// #### Примечание
// Все задания должны быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
//
// #### Литература:
//     - [Деструктуризация на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)
// - [Деструктуризация](https://learn.javascript.ru/destructuring)
    (()=>{
      let a ="spread", b ="rest";
      [a, b] = [b, a]
      console.log(a, b)
    })()