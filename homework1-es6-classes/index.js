// 'use strict'

/**
 ## Завдання
 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
 2. Створіть гетери та сеттери для цих властивостей.
 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

 ## Примітка
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 ## Теоретичне питання
 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?


    1.Новостворені об'єкти здатні використовувати методи свого предка так,
       начебто ці методи його власні. Механізм створення нащадка повинен надавати можливість
        виклику методів та конструктора свого предка
    2.super() використовується для виклику конструктора та методів суперкласу і дозволяє перевизначити їх
 **/



class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    setName(newName) {
        Object.defineProperty(this, 'name',
            {
                value: newName,
                writable: false,
            }
        )
    }

    setAge(newAge) {
        Object.defineProperty(this, 'age',
            {
                value: newAge,
                writable: false,
            }
        )
    }

    setSalary(newSalary) {
        Object.defineProperty(this, 'salary',
            {
                value: newSalary,
                writable: false,
            }
        )
    }

    getName() { return this.name }
    getAge() { return this.age }
    getSalary() { return this.salary }
}

const o = new Employee()

o.setName('Арни')
o.name = 'Вася'              // властивість не може бути встановлена напряму після використання сетера
console.log(o.getName())        //ця особливість буде успадкована підкласом Programmer

class Programmer  extends Employee {
    constructor(name,age,salary,...lang) {
        super(name,age,salary)               //перед this потрібно викликати super для виклику конструктора суперкласу
            this.lang = lang                 //тепер можна додати нову властивість
    }
    setLang(...lang) {
        Object.defineProperty(this, 'lang',
            {
                value: lang,
                writable: false,
            }
        )
    }
    getLang() { return this.lang }
    getSalary() {
        return  super.getSalary() * 3  //super Для модифікації методу. Не обов'язково повинен знаходитися всередині методу,
    }                                   //що модифікується. Може бути на початку у середині чи у кінці
}

//створюємо об'єкти по шаблону підкласу Programmer, визначаємо значення властивостей

const pr1 = new Programmer('Vasia', 20,1000,'en','ja','uk','de');
const pr2 = new Programmer('Petro', 24,3000,'en','uk','de');
const pr3 = new Programmer('Gena', 30,2000,'en');

console.log('/*** створені програмісти по шаблону підкласу Programmer ***/')
console.log(pr1) // У Vasia salary 5000, а lang 'zh','da' так як значення властивості перевизначено сеттером нижче
console.log(pr2) // Petro
console.log(pr3) // Gena


console.log('/*** перевірка суперкласу const w = new Employee() w.setSalary(10) ***/')
const w = new Employee(); w.setSalary(10); console.log( w.getSalary())
console.log(`//w.getSalary() => 10  повертає одинарне значення, прототип не змінився`)
console.log('/*** перевірка суперкласу кінець ***/')

console.log('/*** Programmer.prototype ***/')
console.log(Programmer.prototype)     //методи Employee у прототипі Programmer
console.log('/*** pr1 instanceof Programmer ***/')
console.log(pr1 instanceof Programmer)  //=>'true' p екземпляр підкласу
console.log('/*** pr1 instanceof Employee ***/')
console.log(pr1 instanceof Employee)    //=>'true' p є також екземпляром суперкласу

pr1.setLang('zh','da')          //перевизначили значення властивості сетером

console.log(pr1.getLang())

pr1.setName('Давид')

pr1.name = 'jon' // у строгому режимі помилка у звичайному проігноровано

pr1.setSalary(5000)

console.log('/*** =>\'15000\'   //метод модифіковано - потрійне значення ***/')
console.log(pr1.getSalary())   //

console.log(pr1.getName())     //=>'Давид' успадкувало методи Employee
                             // після сеттера безпосередньо записати властивість не можна
